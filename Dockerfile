FROM node:10

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

RUN npm install --global gulp

COPY package*.json ./
COPY semantic.json .

RUN npm install

COPY . .

COPY --chown=node:node . .

EXPOSE 3000
RUN npm install -g debug@2.6.9
RUN npm install -g nodemon

CMD [ "npm", "run", "watch" ]