# Book Share

# Development
- Clone project
- Run docker
    `docker-compose up -d`
- Access docker
    `docker exec -it book-share bash`
- Load sample data
    `npm run sample`
- Access app url http://localhost
    - Username: admin@gmail.com
    - Password: 123
