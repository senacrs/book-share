const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const mongoose = require("mongoose");
const session = require("express-session");
const MongoStore = require("connect-mongo")(session);
const flash = require("connect-flash");
const passport = require("passport");
const errorHandlers = require("./handlers/errorHandlers");
const indexRouter = require("./routes/index");
const helpers = require('./helpers');

require("./passport");

const app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// Sessions allow us to store data on visitors from request to request
// This keeps users logged in and allows us to send flash messages
app.use(
  session({
    secret: process.env.SECRET,
    key: process.env.KEY,
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: mongoose.connection })
  })
);

// // Passport JS is what we use to handle our logins
app.use(passport.initialize());
app.use(passport.session());

// The flash middleware let's us use req.flash('error', 'Shit!'), which will then pass that message to the next page the user requests
app.use(flash());

// pass variables to templates + all requests
app.use((req, res, next) => {
  res.locals.flashes = req.flash();
  res.locals.loggedUser = req.user || null;
  res.locals.currentPath = req.path;
  res.locals.h = helpers;
  next();
});

app.use("/", indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.use(errorHandlers.flashValidationErrors);

// error handler
app.use(function(err, req, res, next) {
  // if (req.url.indexOf('/admin') >= 0) {
    return errorHandlers.adminErrorHandler(err, req, res, next);
  // }

  // return errorHandlers.apiErrorHandler(err, req, res, next);
});

module.exports = app;
