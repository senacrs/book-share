const passport = require("passport");
const crypto = require("crypto");
const User = require("../../models/User");

/**
 * Show login form
 */
exports.loginForm = (_, res) => res.render('auth/login');

exports.login = passport.authenticate("admin", {
  failureRedirect: "/admin/login",
  failureFlash: "Usuário ou senha incorretos!",
  successRedirect: "/admin",
  successFlash: "Bem vindo!"
});

/**
 * User logout
 */
exports.logout = (req, res) => {
  req.logout();
  res.redirect(res.locals.h.adminUrl());
};

/**
 * Check if user is logged in
 */
exports.isLoggedIn = (req, res, next) => {
  if (req.isAuthenticated()) {
    next();
    return;
  }
  res.redirect(res.locals.h.adminUrl('login'));
};