const Author = require("../../models/Author");

/**
 * List authors
 */
exports.listAuthors = async (_, res) => {
  const authors = await Author.find();
  res.render("authors/list", {
    title: "Autores",
    authors
  });
};

/**
 * Display form to add author
 */
exports.addAuthor = async (_, res) => {
  res.render("authors/add", {
    title: "Adicionar Autor"
  });
};

/**
 * Create author
 */
exports.createAuthor = async (req, res) => {
  const author = await new Author(req.body).save();
  req.flash("success", `${author.name} criado com sucesso.`);
  res.redirect(res.locals.h.adminUrl('authors'));
};

/**
 * Show form to edit an book
 */
exports.editAuthor = async (req, res) => {
  const author = await Author.findById(req.params.id);

  res.render("authors/edit", {
    title: `Editar ${author.name}`,
    author
  });
};

/**
 * Update an book
 */
exports.updateAuthor = async (req, res) => {
  const author = await Author.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  }).exec();

  req.flash("success", `${author.name} atualizado com sucesso.`);
  res.redirect(res.locals.h.adminUrl("authors"));
};

/**
 * Remove author
 */
exports.removeAuthor = async (req, res) => {
  const author = await Author.findByIdAndDelete(req.params.id);
  req.flash("success", `Autor ${author.name} removido com sucesso.`);
  res.redirect(res.locals.h.adminUrl("/authors"));
};
