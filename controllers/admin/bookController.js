const Book = require("../../models/Book");
const Author = require("../../models/Author");

/**
 * List books
 */
exports.listBooks = async (_, res) => {
  const books = await Book.find().populate('author');
  res.render("books/list", {
    title: "Livros",
    books
  });
};

/**
 * Display form to add book
 */
exports.addBook = async (_, res) => {
  const authors = await Author.find();
  res.render("books/add", {
    title: "Adicionar Livro",
    authors
  });
};

/**
 * Create book
 */
exports.createBook = async (req, res) => {
  const book = await new Book(req.body).save();
  req.flash("success", `${book.title} criado com sucesso.`);
  res.redirect(res.locals.h.adminUrl(`books`));
};

/**
 * Show form to edit an book
 */
exports.editBook = async (req, res) => {
  const book = await Book.findById(req.params.id);
  const authors = await Author.find();

  res.render("books/edit", {
    title: `Editar ${book.title}`,
    book,
    authors
  });
};

/**
 * Update an book
 */
exports.updateBook = async (req, res) => {
  const book = await Book.findById(req.params.id);

  book.set(req.body);
  await book.save();

  req.flash("success", `${book.title} atualizado com sucesso.`);
  res.redirect(res.locals.h.adminUrl("books"));
};

/**
 * Remove book
 */
exports.removeBook = async (req, res) => {
  const book = await Book.findByIdAndDelete(req.params.id);
  req.flash("success", `Livro ${book.title} removido com sucesso.`);
  res.redirect(res.locals.h.adminUrl("books"));
};
