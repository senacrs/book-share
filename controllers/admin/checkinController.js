const BookCheckout = require('../../models/BookCheckout');

/**
 * List of checkins
 * @param res
 * @returns {Promise<void>}
 */
exports.list = async (_, res) => {
  const checkins = await BookCheckout.find({returnedDate: {$exists: true }})
    .populate('book', 'title')
    .populate('member', 'name');

  res.render('checkins/list', {
    title: 'Devoluções',
    checkins
  });
};

/**
 * Confirm checkin form
 * @param _
 * @param res
 */
exports.add = async (req, res) => {
  const checkout = await BookCheckout.findById(req.params.checkoutId)
    .populate('book')
    .populate('member');

  res.render('checkins/add', {
    title: 'Nova Devolução',
    checkout,
  });
};

/**
 * Create checkin
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.create = async (req, res) => {
  const body = req.body;
  const checkout = await BookCheckout
    .findById(req.body.checkoutId)
    .populate('book');

  checkout.returnedDate = Date.now();
  await checkout.save();

  const book = checkout.book;

  book.availableQuantity = book.availableQuantity + 1;
  await book.save();

  req.flash('success', 'Livro devolvido com sucesso');
  res.redirect(res.locals.h.adminUrl('checkouts'));
};