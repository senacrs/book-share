const BookCheckout = require("../../models/BookCheckout");
const Book = require("../../models/Book");
const Member = require("../../models/Member");

/**
 * List of checkouts
 * @param res
 * @returns {Promise<void>}
 */
exports.list = async (_, res) => {
  const checkouts = await BookCheckout.find({returnedDate: { $exists: false }})
    .populate('book', 'title')
    .populate('member', 'name');

  res.render('checkouts/list', {
    title: 'Empréstimos',
    checkouts
  });
};

/**
 * add checkout form
 * @param _
 * @param res
 */
exports.add = async (_, res) => {
  const books = await Book.find({availableQuantity: {$gte: 1} });
  const members = await Member.find();

  res.render('checkouts/add', {
    title: 'Novo Empréstimo',
    books,
    members
  });
};

/**
 * Add new checkout
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.create = async(req, res) => {
  const { body } = req;
  await new BookCheckout(body).save();
  const book = await Book.findById(req.body.book);
  book.availableQuantity = book.availableQuantity - 1;
  await book.save();

  req.flash("success", `Empréstimo realizado com sucesso.`);
  res.redirect(res.locals.h.adminUrl(`checkouts`));
};