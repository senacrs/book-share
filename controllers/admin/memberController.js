const Member = require("../../models/Member");
const { validationResult } = require("express-validator");

/**
 * List members
 */
exports.listMembers = async (_, res) => {
  const members = await Member.find();
  res.render("members/list", {
    title: "Membros",
    members
  });
};

/**
 * Display form to add author
 */
exports.addMember = async (_, res) => {
  res.render("members/add", {
    title: "Adicionar Membro"
  });
};

/**
 * Create member
 */
exports.createMember = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    req.flash("error", errors.array().map(err => err.msg));
    res.locals.member = req.body;
    res.locals.flashes = req.flash();
    return this.addMember(req, res);
  }

  const { password, ...body } = req.body;
  const member = new Member(body);

  await Member.register(member, password);

  req.flash("success", `${member.name} criado com sucesso.`);
  res.redirect(res.locals.h.adminUrl(`members`));
};

/**
 * Show form to edit an member
 */
exports.editMember = async (req, res) => {
  const member = await Member.findById(req.params.id);

  res.render("members/edit", {
    title: `Editar Membro`,
    member
  });
};

/**
 * update member
 */
exports.updateMember = async (req, res) => {
  const errors = validationResult(req);

  if (! errors.isEmpty()) {
    req.flash("error", errors.array().map(err => err.msg));
    res.locals.member = req.body;
    res.locals.flashes = req.flash();
    return this.editMember(req, res);
  }
  const { password, ...body } = req.body;

  const member = await Member.findById(req.params.id);

  member.set(body);

  if (password) {
    await member.setPassword(password);
    member.resetPasswordToken = undefined;
    member.resetPasswordExpires = undefined;
  }

  await member.save();

  req.flash("success", "Membro atualizado com sucesso!");
  res.redirect("back");
};

/**
 * Remove member
 */
exports.removeMember = async (req, res) => {
  await Member.findByIdAndDelete(req.params.id);
  req.flash("success", `Membro removido com sucesso.`);
  res.redirect(res.locals.h.adminUrl("members"));
};
