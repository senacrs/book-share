const User = require("../../models/User");
const { validationResult } = require("express-validator");

/**
 * List users
 */
exports.listUsers = async (_, res) => {
  const users = await User.find();
  res.render("users/list", {
    title: "Usuários",
    users
  });
};

/**
 * Display form to add author
 */
exports.addUser = async (_, res) => {
  res.render("users/add", {
    title: "Adicionar Usuário"
  });
};

/**
 * Create user
 */
exports.createUser = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    req.flash("error", errors.array().map(err => err.msg));
    res.locals.user = req.body;
    res.locals.flashes = req.flash();
    return this.addUser(req, res);
  }

  const user = new User({
    email: req.body.email
  });

  await User.register(user, req.body.password);

  req.flash("success", `${user.email} criado com sucesso.`);
  res.redirect(res.locals.h.adminUrl(`users`));
};

/**
 * Show form to edit an book
 */
exports.editUser = async (req, res) => {
  const user = await User.findById(req.params.id);

  res.render("users/edit", {
    title: `Editar Usuário`,
    user
  });
};

/**
 * update user
 */
exports.updateUser = async (req, res) => {
  const errors = validationResult(req);

  if (! errors.isEmpty()) {
    req.flash("error", errors.array().map(err => err.msg));
    res.locals.user = req.body;
    res.locals.flashes = req.flash();
    return this.editUser(req, res);
  }
  const { email, password } = req.body;

  const user = await User.findById(req.params.id);

  user.email = email || user.email;

  if (password) {
    await user.setPassword(password);
    user.resetPasswordToken = undefined;
    user.resetPasswordExpires = undefined;
  }

  await user.save();

  req.flash("success", "Usuário atualizado com sucesso!");
  res.redirect("back");
};

/**
 * Remove user
 */
exports.removeUser = async (req, res) => {
  await User.findByIdAndDelete(req.params.id);
  req.flash("success", `Usuário removido com sucesso.`);
  res.redirect(res.locals.h.adminUrl("users"));
};
