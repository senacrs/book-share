const passport = require("passport");
const jwt = require("jsonwebtoken");
const jwtConfig = require('../../jwtConfig');

/**
 * Login
 */
exports.login = (req, res, next) => {
  passport.authenticate("member", { session: false }, (_, user, info) => {
    if (info) {
      return res.status(400).json({ message: info.message });
    }

    const token = jwt.sign(
      { id: user.id },
      jwtConfig.secret,
      { expiresIn: jwtConfig.expiresIn}
    );

    return res.json({ token });
  })(req, res, next);
};

/**
 * User profile
 */
exports.me = (req, res) => {
  return res.json(req.user);
}

/**
 * Check if user is logged in
 */
exports.isLoggedIn = (req, res, next) => {
  return passport.authenticate('jwt', { session: false })(req, res, next);
};