const Book = require("../../models/Book");

/**
 * List books
 */
exports.listBooks = async (req, res) => {
  let requirements = {};

  if (req.query.search) {
    requirements = { $text: { $search: req.query.search } };
  }

  if (req.query.available == 'true') {
    requirements = {
      ...requirements,
      availableQuantity: { $gte: 1 }
    };
  }

  const books = await Book.find(requirements).populate("author");
  res.json({ books });
};