const { validationResult } = require("express-validator");
const BookCheckout = require('../../models/BookCheckout');

/**
 * Create checkin
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.create = async (req, res) => {
  try {
    validationResult(req).throw();

    const checkout = await BookCheckout
      .findById(req.body.checkoutId)
      .populate('book');

    checkout.returnedDate = Date.now();
    await checkout.save();

    const book = checkout.book;

    book.availableQuantity = book.availableQuantity + 1;

    await book.save();

    return res.json({ message: 'Livro devolvido com sucesso' });
  } catch (error) {
    return res.status(400).json({ errors: error.mapped() });
  }
};

/**
 * List of checkins
 * @param res
 * @returns {Promise<void>}
 */
exports.list = async (req, res) => {
  const member = req.user;
  const checkins = await BookCheckout.find({
    returnedDate: { $exists: true },
    member: member.id
  }).populate('book', 'title');

  return res.json({
    checkins
  });
};