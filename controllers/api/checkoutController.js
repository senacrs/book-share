const { validationResult } = require("express-validator");
const BookCheckout = require("../../models/BookCheckout");
const Book = require("../../models/Book");

/**
 * List of checkouts
 * @param res
 * @returns {Promise<void>}
 */
exports.list = async (req, res) => {
  const member = req.user;
  const checkouts = await BookCheckout.find({
    returnedDate: { $exists: false },
    member: member.id
  }).populate('book', 'title');

  return res.json({
    checkouts
  })
};

/**
 * Add new checkout
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.create = async(req, res) => {
  try {
    validationResult(req).throw();
    const bookId = req.body.book;

    await new BookCheckout({
      book: bookId,
      member: req.user
    }).save();

    const book = await Book.findById(bookId);
    book.availableQuantity = book.availableQuantity - 1;

    await book.save();

    return res.json({
      message: "Empréstimo realizado com sucesso"
    });
  } catch (exception) {
    return res.status(400).json({
      errors: exception.mapped()
    });
  }
};