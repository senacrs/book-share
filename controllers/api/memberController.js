const Member = require("../../models/Member");
const { validationResult } = require("express-validator");

/**
 * Create member
 */
exports.create = async (req, res) => {
  try {
    validationResult(req).throw();
    const { password, ...body } = req.body;
    const member = new Member(body);

    await Member.register(member, password);

    return res.json({
      message: `${member.name} criado com sucesso.`,
    });
  } catch (exception) {
    return res.status(400).json({
      errors: exception.mapped()
    });
  }
};

/**
 * update member
 */
exports.update = async (req, res) => {
  try {
    validationResult(req).throw();
    const { password, ...body } = req.body;

    const member = await Member.findById(req.user.id);

    member.set(body);

    if (password) {
      await member.setPassword(password);
      member.resetPasswordToken = undefined;
      member.resetPasswordExpires = undefined;
    }

    await member.save();

    return res.json({
      message: "Membro atualizado com sucesso!"
    });
  } catch (exception) {
    return res.status(400).json({
      errors: exception.mapped()
    });
  }
};
