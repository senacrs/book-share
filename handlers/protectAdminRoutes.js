const authController = require("../controllers/admin/authController");

module.exports = (publicRoutes = []) => {
  return (req, res, next) => {
    return publicRoutes.includes(req.path)
      ? next()
      : authController.isLoggedIn(req, res, next);
  };
};
