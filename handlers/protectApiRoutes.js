const authController = require("../controllers/api/authController");

module.exports = (publicRoutes = []) => {
  return (req, res, next) => {
    return publicRoutes.includes(req.path)
      ? next()
      : authController.isLoggedIn(req, res, next);
  };
};
