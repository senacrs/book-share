// moment.js is a handy library for displaying dates. We need this in our templates to display things like "Posted 5 minutes ago"
exports.moment = require("moment");

// handy function to get admin url
exports.adminUrl = segment => {
  let baseUrl = "/admin";
  if (segment) {
    baseUrl += `/${segment}`;
  }
  return baseUrl;
};
