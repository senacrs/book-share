const mongoose = require("mongoose");

const authorSchema = new mongoose.Schema({
  name: {
    type: String,
    required: 'O nome do autor é de preenchimento obrigatório',
    trim: true
  }
});

authorSchema.index({
  name: "text"
});

module.exports = mongoose.model("Author", authorSchema);
