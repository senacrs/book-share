const mongoose = require("mongoose");
const slug = require("slugs");

const whenModified = (document, property, callback) => {
  if (document.isModified(property)) {
    return callback();
  }
};

const bookSchema = new mongoose.Schema({
  title: {
    type: String,
    trim: true,
    required: "O título é de preenchimento obrigatório"
  },
  shortDescription: {
    type: String,
    trim: true,
    required: "A descrição é de preenchimento obrigatória"
  },
  thumbnailUrl: {
    type: String,
    trim: true
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Author",
    required: "O autor é de preenchimento obrigatório"
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  slug: String,
  quantity: {
    type: mongoose.Schema.Types.Number,
    required: "A quantidade de items é obrigatória"
  },
  availableQuantity: {
    type: mongoose.Schema.Types.Number
  }
});

bookSchema.index(
  {
    title: "text",
    shortDescription: "text"
  },
  {
    weights: {
      title: 5,
      shortDescription: 1
    }
  }
);

bookSchema.pre("save", async function(next) {
  if (this.isNew) {
    this.availableQuantity = this.quantity;
  }

  await whenModified(this, 'title', async () => {
    this.slug = slug(this.title);

    const slugRegRex = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, "i");
    const booksWithSlug = await this.constructor.find({ slug: slugRegRex });

    if (booksWithSlug.length) {
      this.slug = `${this.slug}-${booksWithSlug.length + 1}`;
    }
  });

  whenModified(this, 'quantity', () => {
    this.availableQuantity = this.availableQuantity + (this.quantity - this.availableQuantity);
  });

  next();
});

module.exports = mongoose.model("Book", bookSchema);
