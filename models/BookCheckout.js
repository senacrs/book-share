const mongoose = require("mongoose");
const moment = require('moment');
const Schema = mongoose.Schema;

const bookCheckoutSchema = new Schema({
  book: {
    type: Schema.Types.ObjectId,
    ref: "Book",
    required: "O livro é de preenchimento obrigatório"
  },
  member: {
    type: Schema.Types.ObjectId,
    ref: "Member",
    required: "O solicitante é de preenchimento obrigatório"
  },
  createdAt: {
    type: Schema.Types.Date,
    default: Date.now()
  },
  dueDate: {
    type: Schema.Types.Date
  },
  returnedDate: {
    type: Schema.Types.Date
  }
});

bookCheckoutSchema.pre('save', function(next) {
  this.dueDate = moment().add(15, 'days');
  next();
});

module.exports = mongoose.model("BookCheckout", bookCheckoutSchema);
