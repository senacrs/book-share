const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const validator = require('validator');
const mongodbErrorHandler = require('mongoose-mongodb-errors');
const passportLocalMongoose = require('passport-local-mongoose');

const addressSchema = new Schema({
  cep: {
    type: Schema.Types.String
  },
  city: {
    type: Schema.Types.String
  },
  state: {
    type: Schema.Types.String,
  },
  district: {
    type: Schema.Types.String,
  },
  number: {
    type: Schema.Types.String,
  },
  street: {
    type: Schema.Types.String
  }
});
const memberSchema = new Schema({
  name: {
    type: Schema.Types.String,
    required: 'O nome é de preenchimento obrigatório'
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    validate: [validator.isEmail, 'E-mail inválido'],
    required: 'O e-mail é de preenchimento obrigatório'
  },
  address: addressSchema,
  resetPasswordToken: String,
  resetPasswordExpires: Date
});

memberSchema.plugin(passportLocalMongoose, { usernameField: 'email' });
memberSchema.plugin(mongodbErrorHandler);

module.exports = mongoose.model('Member', memberSchema);
