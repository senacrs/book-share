const passport = require("passport");
const passportJWT = require("passport-jwt");
const jwtConfig = require("./jwtConfig");
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

const User = require('./models/User');
const Member = require('./models/Member');

passport.use('admin', User.createStrategy());
passport.use('member', Member.createStrategy());
passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: jwtConfig.secret
    },
    async function(jwtPayload, cb) {
      try {
        const member = await Member.findById(jwtPayload.id);
        return cb(null, member);
      } catch (err) {
        return cb(err);
      }
    }
  )
);


passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  if (user != null) done(null, user);
});
