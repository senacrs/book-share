const express = require("express");
const router = express.Router();

const { catchErrors } = require("../../handlers/errorHandlers");
const authorController = require("../../controllers/admin/authorController");

/* List authors */
router.get("/", catchErrors(authorController.listAuthors));

/* Create author */
router.get("/add", catchErrors(authorController.addAuthor));
router.post("/create", catchErrors(authorController.createAuthor));

/* Edit author */
router.get("/:id/edit", catchErrors(authorController.editAuthor));
router.post("/:id/update", catchErrors(authorController.updateAuthor));

/** Remove book */
router.get("/:id/remove", catchErrors(authorController.removeAuthor));

module.exports = router;
