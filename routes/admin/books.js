const express = require('express');
const router = express.Router();

const { catchErrors } = require('../../handlers/errorHandlers');
const bookController = require('../../controllers/admin/bookController');

/* List books. */
router.get('/', catchErrors(bookController.listBooks));

/** Add book */
router.get('/add', bookController.addBook);
router.post('/create', catchErrors(bookController.createBook));

/** Edit book */
router.get('/:id/edit', catchErrors(bookController.editBook));
router.post('/:id/update', catchErrors(bookController.updateBook));

/** Remove book */
router.get('/:id/remove', catchErrors(bookController.removeBook));

module.exports = router;
