const express = require('express');
const router = express.Router();

const { catchErrors } = require('../../handlers/errorHandlers');
const checkinController = require('../../controllers/admin/checkinController');

/* List checkins */
router.get('/', catchErrors(checkinController.list));

/* Checkin an book. */
router.get('/:checkoutId', checkinController.add);
router.post('/create', catchErrors(checkinController.create));

module.exports = router;
