const express = require('express');
const router = express.Router();

const { catchErrors } = require('../../handlers/errorHandlers');
const checkoutController = require('../../controllers/admin/checkoutController');
const checkoutValidator = require('../../validators/checkoutValidator');
/* List checkouts. */
router.get('/', catchErrors(checkoutController.list));

router.get('/add', checkoutController.add);
router.post('/create',
  checkoutValidator.validateBookAndMember,
  catchErrors(checkoutController.create)
);

module.exports = router;
