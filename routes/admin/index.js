const express = require("express");
const router = express.Router();
const protectRoutes = require("../../handlers/protectAdminRoutes");

router.use("/", protectRoutes(["/login"]));
router.use("/", require("./auth"));
router.get("/", (_, res) => {
  res.redirect(res.locals.h.adminUrl("books"));
});

router.use("/users", require("./users"));
router.use("/books", require("./books"));
router.use("/members", require("./members"));
router.use("/authors", require("./authors"));
router.use("/checkouts", require("./checkouts"));
router.use("/checkins", require("./checkins"));

module.exports = router;
