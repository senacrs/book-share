var express = require("express");
var router = express.Router();

const { catchErrors } = require("../../handlers/errorHandlers");
const memberController = require("../../controllers/admin/memberController");
const memberValidator = require("../../validators/memberValidator");

/* List members */
router.get("/", catchErrors(memberController.listMembers));

/* Create member */
router.get("/add", catchErrors(memberController.addMember));
router.post(
  "/create",
  memberValidator.validateRegister,
  catchErrors(memberController.createMember)
);

/* Edit member */
router.get("/:id/edit", catchErrors(memberController.editMember));
router.post("/:id/update",
  memberValidator.validateUpdate,
  catchErrors(memberController.updateMember)
);

/** Remove member */
router.get("/:id/remove", catchErrors(memberController.removeMember));

module.exports = router;
