var express = require("express");
var router = express.Router();

const { catchErrors } = require("../../handlers/errorHandlers");
const userController = require("../../controllers/admin/userController");
const userValidator = require("../../validators/userValidator");
/* List users */
router.get("/", catchErrors(userController.listUsers));

/* Create user */
router.get("/add", catchErrors(userController.addUser));
router.post(
  "/create",
  userValidator.validateRegister,
  catchErrors(userController.createUser)
);

/* Edit user */
router.get("/:id/edit", catchErrors(userController.editUser));
router.post("/:id/update",
  userValidator.validateUpdate,
  catchErrors(userController.updateUser)
);

/** Remove user */
router.get("/:id/remove", catchErrors(userController.removeUser));

module.exports = router;
