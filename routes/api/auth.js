const express = require("express");
const router = express.Router();
const authController = require("../../controllers/api/authController");
const memberController = require("../../controllers/api/memberController");
const memberValidator = require("../../validators/memberValidator");

router.post("/login", authController.login);
router.post(
  "/register",
  memberValidator.validateRegister,
  memberController.create
);

router.get('/me', authController.me);

router.put(
  '/me',
  memberValidator.validateUpdate,
  memberController.update
);
// router.get("/logout", authController.logout);

module.exports = router;
