const express = require("express");
const router = express.Router();

const { catchErrors } = require("../../handlers/errorHandlers");
const bookController = require('../../controllers/api/bookController');

/* List books. */
router.get("/", catchErrors(bookController.listBooks));

module.exports = router;
