const express = require("express");
const router = express.Router();
const checkinController = require("../../controllers/api/checkinController");
const checkinValidator = require("../../validators/checkinValidator");

/** Create checkin */
router.post("/", checkinValidator.validate, checkinController.create);
router.get("/",  checkinController.list);

module.exports = router;
