const express = require("express");
const router = express.Router();
const checkoutController = require("../../controllers/api/checkoutController");
const checkoutValidator = require("../../validators/checkoutValidator");

/* List checkouts. */
router.get("/", checkoutController.list);

/** Create checkout */
router.post("/",
  checkoutValidator.validateBook,
  checkoutController.create
);

module.exports = router;
