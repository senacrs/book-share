const express = require("express");
const router = express.Router();
const protectRoutes = require('../../handlers/protectApiRoutes');
const cors = require("cors");

router.use('/', cors());
router.use("/", protectRoutes(["/login", "/register"]));

router.use("/", require("./auth"));
router.use("/books", require("./books"));
router.use("/checkouts", require("./checkouts"));
router.use("/checkins", require("./checkins"));

module.exports = router;
