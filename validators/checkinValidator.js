const { check } = require("express-validator");
const BookCheckout = require("../models/BookCheckout");

/**
 * Common validation rules
 */
exports.validate = [
  check("checkoutId")
    .exists()
    .withMessage("O identificador do checkout é obrigatório")
    .custom(async (checkoutId, { req }) => {
      if (!checkoutId.match(/^[0-9a-fA-F]{24}$/)) {
        return Promise.reject('Identificador do checkout inválido');
      }

      const checkout = await BookCheckout.findById(checkoutId);

      if (!checkout) {
        return Promise.reject('Registro de checkout não encontrado');
      }

      if (checkout.returnedDate) {
        return Promise.reject('O livro já foi devolvido');
      }

      if (checkout.member != req.user.id) {
        return Promise.reject('O livro só pode ser devolvido pelo seu responsável');
      }
    }),
];
