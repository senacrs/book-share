const { check } = require("express-validator");
const Book = require('../models/Book');

/**
 * Common validation rules
 */
const common = [
  check("book")
    .exists()
    .withMessage("O campo livro é de preenchimento obrigatório")
    .custom(async value => {
      const book = await Book.findById(value);
      if (!book || book.availableQuantity <= 0) {
        return Promise.reject();
      }
    })
    .withMessage("Nenhum exemplar disponível")
];

/**
 * Validate checkout book and member
 * @type {Array}
 */
exports.validateBookAndMember = [
  ...common,
  check('member')
    .exists()
    .withMessage('O campo membro é de preenchimento obrigatório')
];

/**
 * Validate checkout book
 * @type {Array}
 */
exports.validateBook = common;