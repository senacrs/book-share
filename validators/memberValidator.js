const { check, sanitize } = require("express-validator");
const Member = require("../models/Member");

const common = [
  sanitize("email").normalizeEmail({
    remove_dots: false,
    remove_extension: false,
    gmail_remove_subaddress: false
  }),
  check('name')
    .exists()
    .withMessage('O campo nome é de preenchimento obrigatório'),
  check("email")
    .isEmail()
    .withMessage("O campo e-mail não é válido")
    .custom(async function(value, { req }) {
      const compareToId = req.user && req.user.id || req.params.id;
      const member = await Member.findByUsername(value);
      if (member && member.id !== compareToId) {
        return Promise.reject();
      }
    })
    .withMessage("O campo e-mail possui um valor já utilizado"),
];
/**
 * Register chain
 */
exports.validateRegister = [
  ...common,
  check("password")
    .exists()
    .withMessage("O campo senha é de preenchimento obrigatório"),
  check("passwordConfirmation")
    .exists()
    .withMessage(
      "O campo de confirmação de senha é de preenchimento obrigatório"
    )
    .custom((value, { req }) => value === req.body.password)
    .withMessage("O campo de confirmação de senha deve ser igual a senha")
];

/**
 * Update chain
 */
exports.validateUpdate = [
  ...common,
  check("passwordConfirmation")
    .custom((value, { req }) => value === req.body.password)
    .withMessage("O campo de confirmação de senha deve ser igual a senha")
];